// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', ['ionic'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    //$http.get("http://148.213.102.143:8012/hp_ws_json/vendedores.php")
    //.success(function(response) {$scope.names = response.records;}); 
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.controller('vendedorCtrl', function($scope, $http) {
  // GET
  $http.get("http://148.213.102.143:8012/hp_ws_json/vendedores.php")
  .success(function(response) {$scope.names = response.records;}); 
  
  //variables
  $scope.nombre="";
  $scope.telefono="";
  $scope.usuario="";
  $scope.contrasena="";
  $scope.correo="";
  
  var cargarvendedores = function() {
    $http.get("http://148.213.102.143:8012/hp_ws_json/vendedores.php")
    .success(function(response) {$scope.names = response.records;
    alert("Registros cargados UNO");}); 
  };
  
  $scope.GuardarVendedor = function() {
    $http.get("http://148.213.102.143:8012/hp_ws_json/guardarvendedor.php?"+'nombre='+$scope.nombre+'&'+'usuario='+$scope.usuario+'&'+'contrasena='+$scope.contrasena+'&'+'telefono='+$scope.telefono+'&'+'correo='+$scope.correo ).success(function(response) {alert("Registro guardado");
    $http.get("http://148.213.102.143:8012/hp_ws_json/vendedores.php")
    .success(function(response) {$scope.names = response.records;
    alert("Registros cargados TRES");});
    });
  };
  
  $scope.guardar = function() {
    return 'nombre='+$scope.nombre+'&'+'usuario='+$scope.usuario+'&'+'contrasena='+$scope.contrasena+'&'+'telefono='+$scope.telefono+'&'+'correo='+$scope.correo ;
  };
  
  $scope.doRefresh = function() {
    $http.get("http://148.213.102.143:8012/hp_ws_json/vendedores.php")
    .success(function(response) {$scope.names = response.records;
      alert("Registros cargados DOS");})
    .finally(function() {
      // Stop the ion-refresher from spinning
      $scope.$broadcast('scroll.refreshComplete');
    });
  };
});
